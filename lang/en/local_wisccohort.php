<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Lang strings.
 *
 * @package    local
 * @subpackage wisccohort
 * @copyright  2014 University of Wisconsin
 * @author     Matt petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$string['pluginname'] = 'WISC cohort sync';
$string['cohortfile'] = 'Cohort definition file';

$string['viewcohorts'] = 'View WISC cohorts';
$string['returntoall'] = 'Return to all WISC cohorts';
$string['nomembers'] = 'There are no users within this cohort';
$string['lastsynced'] = 'Last synced on';
$string['forceupdate'] = 'Force update';
$string['schemavalidationerror'] = 'Schema validation error';
