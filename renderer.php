<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local_wisccohort rendrer
 *
 * @package    local_wisccohort
 * @copyright  2014 University of Wisconsin - Madison - Board of Regents
 * @author     John Hoopes <hoopes@wisc.edu>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;


class local_wisccohort_renderer extends plugin_renderer_base {


    /**
     * echo the cohorts table
     *
     * @param array $cohorts An array of WISC cohorts
     */
    public function show_wisc_cohorts($cohorts){
        global $DB;

        $forceupdateurl = new moodle_url('/local/wisccohort/view.php', array('action'=>'forceupdate'));

        echo html_writer::tag('p', get_string('lastsynced', 'local_wisccohort') . ': ' . date('Y-m-d H:i:s', get_config('local_wisccohort', 'lastsync')) );
        echo $this->output->single_button($forceupdateurl, get_string('forceupdate', 'local_wisccohort'));

        $data = array();
        foreach($cohorts as $cohort) {
            $line = array();
            $line[] = format_string($cohort->name);
            $line[] = s($cohort->idnumber); // All idnumbers are plain text.
            $line[] = format_text($cohort->description, $cohort->descriptionformat);

            $line[] = $DB->count_records('cohort_members', array('cohortid'=>$cohort->id));

            $line[] = html_writer::link(new moodle_url('/local/wisccohort/view.php', array('action'=> 'viewcohort', 'id'=>$cohort->id)),
                                        html_writer::empty_tag('img', array('src'=>$this->output->pix_url('i/users'),
                                                                            'alt'=>get_string('view'), 'class'=>'iconsmall')));

            $data[] = $line;
        }
        $table = new html_table();
        $table->head  = array(get_string('name', 'cohort'), get_string('idnumber', 'cohort'), get_string('description', 'cohort'),
            get_string('memberscount', 'cohort'), get_string('view'));
        $table->colclasses = array('leftalign name', 'leftalign id', 'leftalign description', 'leftalign size', 'centeralign action');
        $table->id = 'cohorts';
        $table->attributes['class'] = 'admintable generaltable';
        $table->data  = $data;
        echo html_writer::table($table);

    }

    /**
     * echo the cohort members table
     *
     * @param array $members An array WISC cohort members
     */
    public function show_cohort_members($members){
        global $DB;

        echo $this->output->single_button('/local/wisccohort/view.php', get_string('returntoall', 'local_wisccohort'));

        if(empty($members)){
            echo $this->output->box(get_string('nomembers', 'local_wisccohort'), 'generalbox nomembers');
            return;
        }

        $data = array();
        foreach($members as $member) {
            $line = array();
            $line[] = s($member->username);
            $line[] = s($member->firstname); // All idnumbers are plain text.
            $line[] = s($member->lastname);
            $line[] = format_string($member->email);

            $line[] = html_writer::link(new moodle_url('/user/view.php', array('course'=> 1, 'id'=>$member->id)),
                html_writer::empty_tag('img', array('src'=>$this->output->pix_url('i/preview'),
                    'alt'=>get_string('preview'), 'class'=>'iconsmall')), array('target'=>'_blank'));

            $data[] = $line;
        }
        $table = new html_table();
        $table->head  = array(get_string('username'), get_string('firstname'), get_string('lastname'),
            get_string('email'), get_string('view'));
        $table->colclasses = array('leftalign username', 'leftalign firstname', 'leftalign lastname', 'leftalign email', 'centeralign action');
        $table->id = 'cohort_members';
        $table->attributes['class'] = 'admintable generaltable';
        $table->data  = $data;
        echo html_writer::table($table);

    }




}