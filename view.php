<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * View WISC cohorts created by this plugin
 *
 * @package    local_wisccohort
 * @copyright  2014 University of Wisconsin System - Board of Regents
 * @author     John Hoopes <hoopes@wisc.edu>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require($CFG->dirroot.'/cohort/lib.php');
require_once($CFG->libdir.'/adminlib.php');
global $PAGE, $OUTPUT;


require_login();
$context = context_system::instance();
require_capability('moodle/cohort:view', $context);

$action = optional_param('action', '', PARAM_ALPHA);
$id     = optional_param('id', 0, PARAM_INT);


$strcohorts = get_string('viewcohorts', 'local_wisccohort');

$PAGE->set_pagelayout('admin');
$PAGE->set_context($context);
$PAGE->set_url('/local/wisccohort/view.php');
$PAGE->set_title($strcohorts);
$PAGE->set_heading($COURSE->fullname);

echo $OUTPUT->header();

$cohorts = local_wisccohort_get_cohorts();

// get the renderer
$renderer = $PAGE->get_renderer('local_wisccohort');



switch($action){
    case 'viewcohort':

        $members = local_wisccohort_get_cohort_members($id);

        $renderer->show_cohort_members($members);

        break;
    case 'forceupdate':

        local_wisccohort_force_update();

        echo $OUTPUT->single_button('/local/wisccohort/view.php', get_string('continue'));
        break;
    default:

        $renderer->show_wisc_cohorts($cohorts);

}


echo $OUTPUT->footer();
