<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Wisc cohort sync lib file
 *
 * @package    local
 * @subpackage wisccohort
 * @copyright  2014 University of Wisconsin
 * @author     Matt petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function local_wisccohort_cron() {
    $progresstrace = new text_progress_trace();

    $cohortmanager = \local_wisccohort\cohort_manager::instance();
    $cohortmanager->sync_cohorts($progresstrace);

    $progresstrace->finished();
}

function local_wisccohort_force_update(){
    $progresstrace = new html_progress_trace();

    $cohortmanager = \local_wisccohort\cohort_manager::instance();
    $cohortmanager->sync_cohorts($progresstrace, true);

    $progresstrace->finished();
}

function local_wisccohort_extend_settings_navigation($settingsnav, $context){
    global $PAGE;

    if(!has_capability('moodle/cohort:view', $context) ){
        return;
    }

    if($useraccountsnode = $settingsnav->find('accounts', navigation_node::TYPE_SETTING)){

        $linktext = get_string('viewcohorts', 'local_wisccohort');
        $url = new moodle_url('/local/wisccohort/view.php');
        $vcnode = navigation_node::create(
            $linktext,
            $url,
            navigation_node::NODETYPE_LEAF,
            'local_wisccohort',
            'local_wisccohort_view',
            new pix_icon('i/cohort', $linktext)
        );
        if($PAGE->url->compare($url, URL_MATCH_BASE)) {
            $vcnode->make_active();
        }
        $useraccountsnode->add_node($vcnode, 'tooluploaduser');
    }

}



/**
 * Get all the cohorts defined with the wisc_cohort component.
 *
 * @return array
 */
function local_wisccohort_get_cohorts() {
    global $DB;

    return $DB->get_records('cohort', array('component'=>'local_wisccohort'));
}

/**
 *
 *
 * @param int $id
 *
 * @return array
 */
function local_wisccohort_get_cohort_members($id){
    global $DB;

    $fields = array('u.id', 'firstname', 'lastname', 'username', 'email');

    $sql = 'SELECT ' . implode(', ', $fields) . ' FROM {cohort_members} AS cm INNER JOIN {user} AS u ON cm.userid = u.id WHERE ' .
            'cohortid = ? ORDER BY lastname';

    return $DB->get_records_sql($sql, array($id));

}

