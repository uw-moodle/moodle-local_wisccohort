<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines classes used for cohort sync
 *
 * @package   local/wisccohort
 * @copyright 2014 University of Wisconsin
 * @author    Matt petro
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_wisccohort;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/cohort/lib.php');
require_once($CFG->dirroot.'/local/wiscservices/lib/peoplepicker.php');
require_once($CFG->dirroot.'/local/wiscservices/locallib.php');
require_once($CFG->libdir.'/weblib.php');
require_once($CFG->dirroot.'/enrol/wisc/lib.php');
require_once($CFG->dirroot.'/enrol/wisc/controller.class.php');

/**
 * \local_wisccohort\cohort_manager class
 *
 * Singleton class to handle cohort sync
 *
 * @package    local
 * @subpackage wisccohort
 * @copyright  2014 University of Wisconsin
 * @author     Matt petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class cohort_manager {

    protected static $singletoninstance;

    /** @var progress_trace $progresstrace A progress trace class to output progress isntead of using mtrace */
    protected $progresstrace;


    /**
     * Direct initiation not allowed, use the factory method {@link self::instance()}
     */
    protected function __construct() {
    }

    /**
     * Sorry, this is singleton
     */
    protected function __clone() {
    }

    /**
     * Factory method for this class
     *
     * @return core_plugin_manager the singleton instance
     */
    public static function instance() {
        if (is_null(self::$singletoninstance)) {
            self::$singletoninstance = new self();
        }
        return self::$singletoninstance;
    }

    /**
     * Sync cohorts from file
     */
    public function sync_cohorts($progresstrace, $force = false) {
        global $CFG;
        $this->progresstrace = $progresstrace;

        $this->progresstrace->output("Updating WISC cohorts");
        $xmlfile = get_config('local_wisccohort', 'cohortfile');
        if (empty($xmlfile)) {
            $this->progresstrace->output('No XML file found');
            return;
        }
        $this->progresstrace->output("Definition file '$xmlfile'");
        $stat = stat($xmlfile);
        if (!$stat) {
            $this->progresstrace->output("Error: Unable to stat");
            return;
        }
        $schemafile = $CFG->dirroot.'/local/wisccohort/MoodleCohorts.xsd';
        if (stat($schemafile)) {
            $this->progresstrace->output("Using schema file '$schemafile'");
        } else {
            $this->progresstrace->output("Warning: No schema at '$schemafile'");
            $schemafile = null;
        }

        $mtime = $stat['mtime'];
        $lastsync = get_config('local_wisccohort', 'lastsync');

        if ($force || $mtime > $lastsync) {
            $now = time();
            try {
                $this->update_all_cohorts($xmlfile, $schemafile);
                set_config('lastsync', $now, 'local_wisccohort');
                $this->progresstrace->output('Successfully updated cohorts');
            } catch (\SoapFault $e) {
                // Catch soap exceptions so that these don't terminate cron
                $this->progresstrace->output($e->getMessage());
                $this->progresstrace->output($e->getTraceAsString());
            } catch (schema_validation_exception $e) {
                // We might as well update lastsync as there is no reason to keep trying on a validation error.
                $this->progresstrace->output('Fatal error: Schema validation exception');
                set_config('lastsync', $now, 'local_wisccohort');
            }
        } else {
            $this->progresstrace->output("Definition file hasn't changed.  Skipping sync.");
        }
    }

    /**
     * Update or create cohorts based on an xml cohort definition
     *
     * @param string $xmlfile
     * @throws \moodle_exception
     * @throws \coding_exception
     */
    public function update_all_cohorts($xmlfile, $schemafile = null) {
        global $DB;

        $this->progresstrace->output("Processing cohort definition");

        $systemcontext = \context_system::instance();

        // Validate against xsd.

        // This is a bit of a waste to parse the xml twice, but simplexml_load_file doesn't offer schema
        // validation as far as I know.
        if ($schemafile) {
            libxml_use_internal_errors(true);
            $validator = new \DOMDocument;
            $validator->load($xmlfile);
            if (!$validator->schemaValidate($schemafile)) {
                $this->progresstrace->output("Schema validation failed.");
                $errors = libxml_get_errors();
                foreach ($errors as $error) {
                    $errorstr = "";
                    switch ($error->level) {
                        case LIBXML_ERR_WARNING:
                            $errorstr .= "Warning ";
                            break;
                        case LIBXML_ERR_ERROR:
                            $errorstr .= "Error ";
                            break;
                        case LIBXML_ERR_FATAL:
                            $errorstr .= "Fatal Error ";
                            break;
                    }
                    $errorstr .= "$error->code: ".trim($error->message);
                    if ($error->file) {
                        $errorstr .=    " in $error->file";
                    }
                    $errorstr .= " on line $error->line \n";

                    $this->progresstrace->output($errorstr);
                }
                libxml_clear_errors();

                throw new schema_validation_exception('schemavalidationerror', 'local_wisccohort') ;
            }
            $this->progresstrace->output("Schema validated");
        }

        $xml = simplexml_load_file($xmlfile);
        if (!$xml) {
            throw new \moodle_exception("Unable to read cohort file $xmlfile");
        }

        $allcohortids = array();
        foreach ($xml as $xmlcohort) {
            if ($xmlcohort->attributes()->idnumber == 'allcoursecreators') {
                // This is the special pseudo-cohort for all course creators
                // This isn't a real cohort, so all we need to handle is wisc enrollment permissions
                $allcohortids[] = 0; // Record for later as cohort id 0
                $this->update_wisc_access(null, $xmlcohort);
                $this->update_allcoursecreator_overrides($xmlcohort);
                continue;
            }

            $cohortid = $this->update_cohort($xmlcohort);
            if (!$cohortid) {
                continue;
            }
            $allcohortids[] = $cohortid;

            $members = $this->update_cohort_members($cohortid, $xmlcohort);

            $this->update_category_roles($cohortid, $members, $xmlcohort);

            $this->update_system_roles($cohortid, $members, $xmlcohort);

            $this->update_wisc_access($cohortid, $xmlcohort);
        }

        $this->purge_other_cohorts($allcohortids);
    }

    protected function update_cohort($xmlcohort) {
        global $DB;
        $systemcontext = \context_system::instance();

        $cohort = new \stdClass();
        $cohort->name = (string)$xmlcohort->name;
        $cohort->idnumber = (string)$xmlcohort->attributes()->idnumber;
        $cohort->description = (string)$xmlcohort->description;
        $cohort->component = 'local_wisccohort';
        $cohort->contextid = $systemcontext->id;

        if (empty($cohort->idnumber)) {
            $this->progresstrace->output("Missing idnumber in cohort $cohort->name");
            return null;
        }

        // Process cohort record
        $params = array('idnumber'=>$cohort->idnumber, 'contextid'=>$cohort->contextid, 'component'=>'local_wisccohort');
        $cohortid = $DB->get_field('cohort', 'id', $params, IGNORE_MISSING);
        if ($cohortid !== false) {
            $cohort->id = $cohortid;
            $this->progresstrace->output("Updating cohort $cohort->name");
            cohort_update_cohort($cohort);
        } else {
            $this->progresstrace->output("Creating new cohort $cohort->name");
            $cohortid = cohort_add_cohort($cohort);

        }
        return $cohortid;
    }

    protected function update_cohort_members($cohortid, $xmlcohort) {
        global $DB;
        // Process cohort members
        $members = array();
        foreach ($xmlcohort->members->user as $user) {
            $username = (string) $user->attributes()->username;
            if (is_null($username)) {
                // we must match on username, so skip
                $this->progresstrace->output('Missing username, skipping.');
                continue;
            }
            $userid = $this->find_or_add_user($username);
            if ($userid !== false) {
                $members[] = $userid;
            }
        }

        $existing = $DB->get_records_menu('cohort_members', array('cohortid'=>$cohortid), '', 'id, userid');
        // add new members
        foreach (array_diff($members, $existing) as $userid) {
            cohort_add_member($cohortid, $userid);
        }
        // remove any extra members
        foreach (array_diff($existing, $members) as $userid) {
            cohort_remove_member($cohortid, $userid);
        }
        return $members;
    }

    protected function update_category_roles($cohortid, $members, $xmlcohort) {
        global $DB;
        // Process category role assignments
        $allcats = array();
        foreach ($xmlcohort->role_assignments->category as $category) {
            $catidnumber = (string) $category->attributes()->idnumber;
            $roleshortname = (string) $category->attributes()->role;
            if (is_null($catidnumber) || is_null($roleshortname)) {
                $this->progresstrace->output('Missing category idnumber or role, skipping.');
                continue;
            }
            $catid = $this->find_or_add_category($catidnumber);
            if ($catid === false) {
                $this->progresstrace->output("Unable to create category with idnumber $catidnumber");
                continue;
            }
            $roleid = $DB->get_field('role', 'id', array('shortname'=>$roleshortname), IGNORE_MISSING);
            if ($roleid === false) {
                $this->progresstrace->output("Unable to locate role with shortname $roleshortname");
                continue;
            }

            $allcats[$catid][] = $roleid;
            $allcatcontexts[] = \context_coursecat::instance($catid)->id;
        }

        foreach ($allcats as $catid=>$roles) {
            $this->update_role_assignments($cohortid, $members, \context_coursecat::instance($catid), $roles);
        }

        // remove any assignments in other categories for this cohort
        $allcatcontexts = array();
        foreach (array_keys($allcats) as $catid) {
            $allcatcontexts[] = \context_coursecat::instance($catid)->id;
        }
        // Use -1 if array is empty, as that will match no context
        list($notinsql, $params) = $DB->get_in_or_equal($allcatcontexts, SQL_PARAMS_NAMED, 'ctx', false, -1);
        $params['component'] = 'local_wisccohort';
        $params['itemid'] = $cohortid;
        $sql = "contextid $notinsql AND component = :component AND itemid = :itemid";
        $recs = $DB->get_records_select('role_assignments', $sql, $params);
        foreach ($recs as $rec) {
            role_unassign($rec->roleid, $rec->userid, $rec->contextid, $rec->component, $rec->itemid);
        }
    }

    protected function update_system_roles($cohortid, $members, $xmlcohort) {
        global $DB;
        // Process category role assignments
        $allroles = array();
        foreach ($xmlcohort->role_assignments->system as $system) {
            $roleshortname = (string) $system->attributes()->role;
            if (is_null($roleshortname)) {
                $this->progresstrace->output('Missing role, skipping.');
                continue;
            }
            $roleid = $DB->get_field('role', 'id', array('shortname'=>$roleshortname), IGNORE_MISSING);
            if ($roleid === false) {
                $this->progresstrace->output("Unable to locate role with shortname $roleshortname");
                continue;
            }

            $allroles[] = $roleid;
        }

        $this->update_role_assignments($cohortid, $members, \context_system::instance(), $allroles);
    }


    protected function update_wisc_access($cohortid, $xmlcohort) {

        // Process WISC roster access
        if (empty($xmlcohort->wisc_roster_access)) {
            // No access rules
            return;
        }
        $accessrows = array();
        foreach ($xmlcohort->wisc_roster_access->accessrule as $accessxml) {

            $permissions = array('FULL'=>\enrol_wisc_plugin::CREATECOURSE_FULL,'LIMITED'=>\enrol_wisc_plugin::CREATECOURSE_LIMITED);

            $accessrow = new \stdClass();
            $accessrow->cohortid = $cohortid;
            $accessrow->permission = $permissions[(string) $accessxml->attributes()->permission];
            $accessrow->item = (string)$accessxml->attributes()->item;
            $accessrow->type = is_numeric($accessrow->item)? \enrol_wisc_plugin::ACCESS_SUBJECT : \enrol_wisc_plugin::ACCESS_ORG;
            $accessrow->component = 'local_wisccohort';

            if (empty($accessrow->permission) || empty($accessrow->item)) {
                $this->progresstrace->output("Skipping invalid access rule");
                continue;
            }
            $accessrows[] = $accessrow;
        }
        $this->update_wisc_cohort_access($cohortid, $accessrows);
    }

    /**
     * Generate role overrides to allow self-service course creation to work.
     *
     * Limitations:  This code will set, but never remove role overrides.
     *
     * @param unknown $xmlcohort
     */
    protected function update_allcoursecreator_overrides($xmlcohort) {
        global $DB;
        // Process WISC roster access
        $authenticateduserroleid = $DB->get_field('role', 'id', array('shortname'=>'user'));
        $accessrows = array();
        foreach ($xmlcohort->wisc_roster_access->accessrule as $accessxml) {

            $item = (string)$accessxml->attributes()->item;
            if (empty($item)) {
                $this->progresstrace->output("Skipping invalid access rule");
                continue;
            }
            $catid = $this->find_or_add_category($item);
            if ($catid) {
                $catcontext = \context_coursecat::instance($catid);
                role_change_permission($authenticateduserroleid, $catcontext, 'enrol/wisc:createcourseincategory', CAP_ALLOW);
            }
        }
    }

    protected function update_wisc_cohort_access($cohortid, $accessrows) {
        global $DB;

        $records = $DB->get_records('enrol_wisc_access', array('cohortid'=>$cohortid, 'component'=>'local_wisccohort'));
        $makeindexkey = function($rec) { return "$rec->item:$rec->type:$rec->permission"; };

        // index via $indexkey function
        $existing = array();
        foreach ($records as $record) {
            $existing[$makeindexkey($record)] = $record;
        }

        foreach ($accessrows as $row) {
            $key = $makeindexkey($row);
            if (isset($existing[$key])) {
                // already there
                unset($existing[$key]);
            } else {
                // insert new record
                $DB->insert_record('enrol_wisc_access', $row);
            }
        }
        // Delete anything that's left
        foreach ($existing as $rec) {
            $DB->delete_records('enrol_wisc_access', array('id'=>$rec->id));
        }
    }

    protected function update_role_assignments($cohortid, $members, $context, $roleids) {
        global $DB;

        foreach ($roleids as $roleid) {
            $params = array('contextid'=>$context->id, 'roleid'=>$roleid, 'component'=>'local_wisccohort', 'itemid'=>$cohortid);
            $rolecurrentusers = $DB->get_records_menu('role_assignments', $params, '', 'id,userid');

            // assign missing members
            foreach (array_diff($members, $rolecurrentusers) as $userid) {
                role_assign($roleid, $userid, $context->id, 'local_wisccohort', $cohortid);
            }
            // remove extra members
            foreach (array_diff($rolecurrentusers, $members) as $userid) {
                role_unassign($roleid, $userid, $context->id, 'local_wisccohort', $cohortid);
            }
        }

        // Remove any other role assignments we might have had
        if (!empty($roleids)) {
            list ($notinsql, $params) = $DB->get_in_or_equal($roleids, SQL_PARAMS_NAMED, 'roleid', false);
            $rolesql = "roleid $notinsql AND ";
        } else {
            list ($rolesql, $params) = array("", array());
        }
        $params['component'] = 'local_wisccohort';
        $params['itemid'] = $cohortid;
        $params['contextid'] = $context->id;
        $sql = "$rolesql component=:component AND itemid=:itemid AND contextid=:contextid";
        $recs = $DB->get_records_select('role_assignments', $sql, $params);
        foreach ($recs as $rec) {
            role_unassign($rec->roleid, $rec->userid, $rec->contextid, $rec->component, $rec->itemid);
        }

    }

    protected function find_or_add_category($catidnumber) {
        global $DB;

        if (empty($catidnumber)) {
            return false;
        }
        return \enrol_wisc_controller_helper::create_category_by_idnumber($catidnumber);
    }


    protected function find_or_add_user($username) {
        global $DB;

        static $wiscservices = null;
        static $peoplepicker = null;


        if ($userid = $DB->get_field('user', 'id', array('username'=>$username, 'deleted'=>0))) {
                return $userid;  // found user, so done
        }

        if (!$wiscservices) {
            $wiscservices = new \local_wiscservices_plugin();
        }
        if (!$peoplepicker) {
            $peoplepicker = new \wisc_peoplepicker();
        }

        // not found, so try adding by netid
        $netid = null;
        if (preg_match('/^([^@]+)@wisc.edu$/', $username, $matches)) {
            $netid = $matches[1];
        }

        if (!$netid) {
            $this->progresstrace->output("  WARN: Unknown local username: $username");
            return false;
        }

        // query peoplepicker
        $person = $peoplepicker->getPeopleByNetid(array($netid));
        $person = reset($person);

        if ($person) {
            // found someone, so add to moodle
            return $wiscservices->verify_person($person, true);
        }
        $this->progresstrace->output("  WARN: Netid '$netid' not found in UDS");
        return false;
    }

    protected function purge_other_cohorts(array $allcohortids) {
        global $DB;

        // Use -1 if there are no cohortids as it will never match.
        list($notinsql, $params) = $DB->get_in_or_equal($allcohortids, SQL_PARAMS_NAMED, 'cohortid', false, -1);

        // Remove other cohorts
        $params['component'] = 'local_wisccohort';
        $cohorts = $DB->get_records_select('cohort', "id $notinsql AND component=:component", $params);
        foreach ($cohorts as $cohort) {
            $this->progresstrace->output("Removing cohort $cohort->name");
            cohort_delete_cohort($cohort);
        }

        // Purge other role assignments
        $ras = $DB->get_records_select('role_assignments', "itemid $notinsql AND component=:component", $params);
        foreach ($ras as $ra) {
            role_unassign($ra->roleid, $ra->userid, $ra->contextid, $ra->component, $ra->itemid);
        }

        // Purge wisc cohort access entries with no corresponding cohort
        // The coalesce is so that the pseudo allcoursecreator cohort with NULL cohortid can be
        // processed in the same way as other cohorts.
        $DB->delete_records_select('enrol_wisc_access', "COALESCE(cohortid,0) $notinsql AND component=:component", $params);
    }

}
